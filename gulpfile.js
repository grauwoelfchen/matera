'use strict';

var gulp = require('gulp')
  , clean = require('gulp-clean')
  , copy = require('gulp-copy')
  , rename = require('gulp-rename')
  , run = require('run-sequence')
  ;

var jsFw = 'node_modules/jquery/dist'
  , cssFw = 'node_modules/materialize-css/dist'
  , dstDir = 'site'
  ;

// copy node_modules/*/**/css/materialize.min.css into vendor
gulp.task('distribute:materialize-css', function() {
  return gulp.src(cssFw + '/css/materialize.min.css')
  .pipe(copy(dstDir + '/vendor/', {prefix: 3}));
});

// copy node_modules/*/**/js/materialize.min.css into vendor
gulp.task('distribute:materialize-js', function() {
  return gulp.src(cssFw + '/js/materialize.min.js')
  .pipe(copy(dstDir + '/vendor/', {prefix: 3}));
});

// copy node_modules/*/**/fonts/* into vendor
gulp.task('distribute:materialize-font', function() {
  return gulp.src(cssFw + '/fonts/**/*')
  .pipe(copy(dstDir + '/vendor/', {prefix: 3}));
});

var jQueryVer = '2.1.1';

// copy node_modules/*/**/jquery.min.js into vendor
gulp.task('distribute:jquery-js', function() {
  return gulp.src(jsFw + '/jquery.min.js')
  .pipe(rename('jquery-' + jQueryVer + '.min.js'))
  .pipe(gulp.dest(dstDir + '/vendor/js'));
});

// copy node_modules/*/**/jquery.min.map into vendor
gulp.task('distribute:jquery-map', function() {
  return gulp.src(jsFw + '/jquery.min.map')
  .pipe(rename('jquery-' + jQueryVer + '.min.map'))
  .pipe(gulp.dest(dstDir + '/vendor/js'));
});

gulp.task('distribute:all', [
  'distribute:materialize-css'
, 'distribute:materialize-js'
, 'distribute:materialize-font'
, 'distribute:jquery-js'
, 'distribute:jquery-map'
]);

// remove all files into site{/vendor}
gulp.task('clean', function() {
  return gulp.src([
    'site/vendor/*'
  ], {
    read: false
  })
  .pipe(clean());
});

// -- [main tasks]

gulp.task('default', function() {
  var nodeEnv = process.env.NODE_ENV || 'production';
  console.log('» gulp:', nodeEnv);

  return run('clean', ['distribute:all'])
});
