# Matera

`/matera/`

[![build status](
https://gitlab.com/grauwoelfchen/matera/badges/master/build.svg)](
https://gitlab.com/grauwoelfchen/matera/commits/master)

A project for web design practice using [Materialize](
http://materializecss.com/).


## Getting Started

At first, create your project directory (development environment).

### Prepare

```bash
$ cd /path/to/projects
$ mkdir matera
$ cd matera
```

### Setup

You need following steps only at once (first).

#### Create development environment

As example below, you need:

* Python 3.5
* Node.js 7.10.0 (installed via python package `nodeenv`)

##### Setup

###### Virtualenv (python)

```
$ python3.5 -m venv venv
$ source venv/bin/activate
(venv) $ pip install --upgrade setuptools pip
(venv) $ pip install nodeenv
```

###### Node.js

```
(venv) $ nodeenv -p --node=7.10.0
(venv) $ source venv/bin/activate
(venv) $ npm install --global --upgrade npm
```

##### Check versions

```bash
(venv) $ python -V
Python 3.5.2
(venv) $ node --version
v7.10.0
(venv) $ npm --version
5.0.3
```


#### Install

Install dependencies.

```bash
(venv) $ npm install
(venv) $ npm install --global gulp-cli
```

#### Distribute

`gulp` command setups project for you.  

```bash
(venv) $ gulp
```

And then,

```bash
$ tree site
site
├── index.html
├── css
│   └── index.css
├── js
│   └── index.js
└── vendor
    ├── css
    │   └── materialize.min.css
    └── js
        ├── jquery.min.js
        ├── jquery.min.map
        └── materialize.min.js

5 directories, 7 files
```

Now, Let's start!


## License

Matera; Copyright (c) 2017 Yasuhiro Asaka

This program is free software: you can redistribute it and/or modify it
under the terms of the MIT License.

See [LICENSE](LICENSE).
